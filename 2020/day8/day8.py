def stop_at_recursion(stepper):
    def stepper_wrapper(prog, step=0, acc=0):
        steps = set()
        run = stepper(prog, step, acc)
        while len(run['prog']) > run['step']:
            if run['step'] in steps:
                run['err'] = True
                return run
            else:
                steps.add(run['step'])
            run = stepper(**run)
        run['err'] = False
        return run
    return stepper_wrapper


@stop_at_recursion
def stepper(prog, step, acc):
    command, value = prog[step]
    if command == 'nop':
        return {'prog': prog, 'step': step + 1, 'acc': acc}
    elif command == 'acc':
        return {'prog': prog, 'step': step + 1, 'acc': acc + value}
    elif command == 'jmp':
        return {'prog': prog, 'step': step + value, 'acc': acc}


def generate_deltas(prog):
    return [(step, 'jmp', i) if op == 'nop'
            else (step, 'nop', i)
            for step, (op, i) in enumerate(prog)
            if op in ('jmp', 'nop')]


def part_2(prog, prog_deltas):
    prog = list(prog)
    for step, command, value in prog_deltas:
        old_delta = prog[step]
        prog[step] = (command, value)
        ret_vals = stepper(prog)
        prog[step] = old_delta
        if ret_vals['err'] is False:
            return ret_vals


if __name__ == '__main__':
    with open('input_day8', 'r') as steps_inst:
        program = [(step.split()[0], int(step.split()[1]))
                   for step in steps_inst.read().splitlines()]

    print('P1:', stepper(program)['acc'])

    all_deltas = generate_deltas(program)
    print('P2:', part_2(program, all_deltas)['acc'])
